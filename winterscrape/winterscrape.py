#
# Copyright (C) 2017 Chanku/Sapein - All Rights Reserved
# You may use, distribute and modify this code under the
# terms of the MIT license
#
# You should have received a copy of the MIT license with
# this file. If not, please write to: chandler182@gmail.com, or
# visit https://opensource.org/licenses/MIT:
#
"""
This module contains the Public API for winterscrape, as such this is what is to be imported.

.. note::
   All functions can raise a ValueError or urllib error if the URL or if the data isn't proper.
   This is not documented individually for each function, unless the function causes that error.
"""

import datetime
import re
import bs4
from . import _winterscrape


__author__ = "Chanku/Sapein"
__license__ = "MIT"
__version__ = '0.0.1-alpha'
__email__ = "chandler182@gmail.com"

USE_BS4 = False
INDEX_DATA = None
KEEP_UA_INFO = True
RETURN_RAW = False

USER_AGENT = _winterscrape._USER_AGENT
ORIGINAL_URL = _winterscrape._WINTREATH_BASE_URL
LOOP_LIMIT = 999999

def _unique_posters(poster_list):
    id_reg = re.compile(':[0-9]*$')
    new_poster_list = []
    for poster in poster_list:
        poster = id_reg.split(poster)[0]
        new_poster_list.append(poster)

    poster_list = new_poster_list
    poster_set = set(poster_list)
    poster_list = list(poster_set)

    return poster_list

def _get_post_data(post_data):
    post_dict = {}
    for post in post_data:
        post_data = post.find_all('td')
        for post_information in post:
            author = post_information.find('a', title='View the profile of *')
            date_tag = post_information.find_all('div', class_='smalltext')
            post_date, post_time = _get_string_datetime(date_tag)
            post_text = post_information.find('div', class_='post_area')
            post_text_div = post_text.find('div', class_='inner')
            post_text = post_text_div.text
            for br_tag in post_text.find_all('br'):
                br_tag.replace_with('\n')
            post_id = post_text_div['attrs'].id
            post_id.strip("msg_")
            if author in list(post_dict.keys()):
                open_key = False
                count = 1
                while (not open_key) and count < LOOP_LIMIT:
                    new_author = '{author}:{count}'.format(author=author, count=count)
                    if new_author not in list(post_dict.keys()):
                        author = new_author
                        open_key = True
                    count += 1

            post_dict[author] = [(post_date, post_time), post_text, post_id]
    return post_dict

def _search_forum(topic_name, forum_data):
    gotten_data = False
    i = 0
    while not gotten_data or i < LOOP_LIMIT:
        _, forum_data = _winterscrape._access_forum_page(i, forum_data, USER_AGENT, KEEP_UA_INFO)
        try:
            _, ac_data = _winterscrape._access_topic_in_forum(topic_name, forum_data,
                                                              USER_AGENT, KEEP_UA_INFO)
            gotten_data = True
            data = ac_data
        except ValueError:
            gotten_data = False
            i += 1

    return data

def _get_string_datetime(date_information):
    try:
        time = date_information.text.split(' at')[1]
    except IndexError:
        time = date_information.text.split(', ')[2]
    if '<strong>' in str(date_information):
        date = date_information.text
        if date.lower() == 'today':
            date_datetime = datetime.datetime.utcnow()
            date = datetime.datetime.strftime(date.date(), '%B %d, %Y')
        elif date.lower() == 'yesterday':
            date_datetime = datetime.datetime.utcnow() - datetime.timedelta(days=1)
            date = datetime.datetime.strftime(date_datetime.date(), '%B %d, %Y')
    else:
        try:
            date = date.text.split(' at')[0]
        except IndexError:
            date_list = date.text.split(', ')
            new_date = ''
            new_date.join(date_list[:2])
            date = new_date

    time = '{} UTC'.format(time)
    return date, time

def setup_module(use_bs4, give_raw, custom_url=ORIGINAL_URL, user_agent=USER_AGENT,
                 keep_module_ua=KEEP_UA_INFO):
    """
    Changes certain, module values, to change certain behaviors

    :param use_bs4: Whether or not html data should be run through Beautiful Soup 4
    :type use_bs4: bool
    :param give_raw: Whether or not the raw response from the webserver should be returned,
                     where possible.
    :type give_raw: bool
    :param custom_url: A custom URL if you want to use a non-wintreath URL.
    :type custom_url: str
    :param user_agent: A User Agent to be used outside of the module user agent.
    :type user_agent: str
    :param keep_module_ua: Keep the User Agent of Winterscrape, in addition to the given user agent
    :type keep_module_ua: bool

    """

    global USE_BS4
    global USER_AGENT
    global INDEX_DATA
    global KEEP_UA_INFO
    global RETURN_RAW

    USE_BS4 = use_bs4
    RETURN_RAW = give_raw
    KEEP_UA_INFO = keep_module_ua
    INDEX_DATA = _winterscrape._initiate_connection(user_agent, custom_url, keep_module_ua)
    USER_AGENT = _winterscrape._USER_AGENT

def get_forum_page(forum_name, page_number=1):
    """
    Get a specific page from a specific forums.

    .. note:: This does not work with subforums, use get_subforum_page instead

    :param forum_name: The name of the forum you wish to use
    :type forum_name: str
    :param page_number: The page number you wish to access
    :type page_number: int

    :returns: data - either a string or BeautifulSoup class
            raw_data - the bytes version of data, only if you asked for returning raw.
    """

    raw_data, data = _winterscrape._access_forum_area(forum_name, INDEX_DATA,
                                                      USER_AGENT, KEEP_UA_INFO)
    if page_number > 1:
        raw_data, data = _winterscrape._access_forum_page(page_number, data,
                                                          USER_AGENT, KEEP_UA_INFO)

    if USE_BS4:
        data = bs4.BeautifulSoup(data)

    if RETURN_RAW:
        return raw_data, data
    else:
        return data

def get_topic_page(topic_name, page_number=1, forum_name=None, data=None):
    """
    Gets a page for a topic on the forum.

    .. note:: You **MUST** include either the name of the forum, or the html data.

    .. note:: In order to work with subforums, you must pass in the html data instead.

    :param topic_name: The name of the topic you wish to access.
    :type topic_name: str
    :param page_number: The page of the topic you wish to access.
    :type page_number: int
    :param forum_name: The name of the forum that the post is stored.
    :type forum_name: str
    :param data: The HTML data of the forum index
    :type data: str

    :returns: data - either a string or BeautifulSoup class
            raw_data - the bytes version of data, only if you asked for returning raw.
    :raises: SyntaxError

    """
    _, data = _winterscrape._access_forum_area(forum_name, INDEX_DATA, USER_AGENT, KEEP_UA_INFO)
    if forum_name:
        try:
            _, ac_data = _winterscrape._access_topic_in_forum(topic_name, data,
                                                              USER_AGENT, KEEP_UA_INFO)
        except ValueError:
            gotten_data = False
            i = 0
            while not gotten_data or i < LOOP_LIMIT:
                _, data = _winterscrape._access_forum_page(i, data, USER_AGENT, KEEP_UA_INFO)
                try:
                    _, ac_data = _winterscrape._access_topic_in_forum(topic_name, data,
                                                                      USER_AGENT, KEEP_UA_INFO)
                    gotten_data = True
                except ValueError:
                    pass
                i += 1
            data = ac_data

    elif data:
        pass
    else:
        raise SyntaxError("Must pass in forum_name or data")

    raw_data, data = _winterscrape._access_forum_page(page_number, data, USER_AGENT, KEEP_UA_INFO)

    if USE_BS4:
        data = bs4.BeautifulSoup(data)

    if RETURN_RAW:
        return raw_data, data
    else:
        return data

def get_topic(topic_name, forum_name, forum_page=None):
    """
    Gets a topic from the forum, from a given page.

    .. note::
        If no page is given it will search through the forums. This does not work with subforums.

    :param topic_name: The name of the topic you are accessing.
    :type topic_name: str
    :param forum_name: The name of the forum that the topic is in.
    :type forum_name: str
    :param forum_page: The page of the forum that the topic is in.
    :type forum_page: int

    :returns: data - either a string or BeautifulSoup class
              raw_data - the bytes version of data, only if you asked for returning raw.
    :raises: ValueError

    """
    _, data = _winterscrape._access_forum_area(forum_name, INDEX_DATA, USER_AGENT, KEEP_UA_INFO)
    _, data = _winterscrape._access_forum_page(forum_page, data, USER_AGENT, KEEP_UA_INFO)
    if forum_page:
        #Get the forum first, then forum page, then topic
        try:
            raw_data, data = _winterscrape._access_topic_in_forum(forum_page, data,
                                                                  USER_AGENT, KEEP_UA_INFO)
        except ValueError:
            raise ValueError(("Topic ({topic}) not found in forum ({forum}) on page {page}."
                             ).format(topic=topic_name, forum=forum_name, page=forum_page))
    else:
        #Get the forum first, then topic. If topic is not on forum page check others
        try:
            raw_data, ac_data = _winterscrape._access_topic_in_forum(topic_name, data,
                                                                     USER_AGENT, KEEP_UA_INFO)
        except ValueError:
            gotten_data = False
            i = 0
            while not gotten_data or i < LOOP_LIMIT:
                raw_data, data = _winterscrape._access_forum_page(i, data, USER_AGENT, KEEP_UA_INFO)
                try:
                    raw_data, ac_data = _winterscrape._access_topic_in_forum(topic_name,
                                                                             data,
                                                                             USER_AGENT,
                                                                             KEEP_UA_INFO)
                    gotten_data = True
                except ValueError:
                    pass
                i += 1
    try:
        data = ac_data
    except NameError:
        pass

    if USE_BS4:
        data = bs4.BeautifulSoup(data)

    if RETURN_RAW:
        return raw_data, data
    else:
        return data

def get_forum(forum_name):
    """
    Gets the first page of a forum's index.

    .. note:: Does not work with subforums

    :param forum_name: The name of the forum you wish to get
    :type forum_name: str

    :returns: data - either a string or BeautifulSoup class
            raw_data - the bytes version of data, only if you asked for returning raw.
    :raises: SyntaxError

    """
    raw_data, data = _winterscrape._access_forum_area(forum_name, INDEX_DATA,
                                                      USER_AGENT, KEEP_UA_INFO)

    if USE_BS4:
        data = bs4.BeautifulSoup(data)

    if RETURN_RAW:
        return raw_data, data
    else:
        return data

def get_subforum_page(subforum, page_number):
    """
    Gets a given page of a subforum.

    .. note:: Does not work with regular forums

    :param subforum: name of the subforum
    :type subforum: str
    :param page_number: the page of the subforum you want to access
    :type page_number: int

    :returns: data - either a string or BeautifulSoup class
            raw_data - the bytes version of data, only if you asked for returning raw.

    """
    _, data = _winterscrape._access_subforum_from_index(subforum, INDEX_DATA,
                                                        USER_AGENT, KEEP_UA_INFO)

    raw_data, data = _winterscrape._access_forum_page(page_number, data,
                                                      USER_AGENT, KEEP_UA_INFO)

    if USE_BS4:
        data = bs4.BeautifulSoup(data)

    if RETURN_RAW:
        return raw_data, data
    else:
        return data

def get_subforum(subforum, parent=None):
    """
    Gets the first pgae of a subforum.

    .. note:: Does not work with regular forums

    :param subforum: name of the subforum
    :type subforum: str
    :param parent: The name of the parent forum
    :type parent: str

    :returns: data - either a string or BeautifulSoup class
            raw_data - the bytes version of data, only if you asked for returning raw.

    """
    if parent:
        _, parent_data = _winterscrape._access_forum_area(parent, INDEX_DATA,
                                                          USER_AGENT, KEEP_UA_INFO)
        raw_data, data = _winterscrape._access_subforum_from_forum(subforum, parent_data,
                                                                   USER_AGENT, KEEP_UA_INFO)
    else:
        raw_data, data = _winterscrape._access_subforum_from_index(subforum, INDEX_DATA,
                                                                   USER_AGENT, KEEP_UA_INFO)

    if USE_BS4:
        data = bs4.BeautifulSoup(data)

    if RETURN_RAW:
        return raw_data, data
    else:
        return data

def get_posts_from_topic_page(topic_name, page_number, forum_name=None, forum_page=1, data=None):
    """
    Gets the post data from any given topic page.

    .. note:: Does not support subforums.

    .. note:: Either forum_name and forum_page or data must be given, but not both.

    :param topic_name: The name of the topic you wish to access.
    :type topic_name: str
    :param page_number: The page of the forum you wish to access.
    :type page_number: int
    :param forum_name: The name of the forum the topic is located in.
    :type forum_name: str
    :param forum_page: The page that the forum is on.
    :type forum_page: int
    :param data: The data of the forum index that the topic is in.
    :type data: str

    :returns: dict - A dictionary of lists in the following format:
            {'username[:count]':[[Date, Time], Post_Text, Post_ID]}

    :raises: AttributeError, SyntaxError, ValueError
    """
    post_dict = {}
    _, data = _winterscrape._access_forum_area(forum_name, INDEX_DATA, USER_AGENT, KEEP_UA_INFO)
    if forum_name and forum_page == 1:
        try:
            _, ac_data = _winterscrape._access_topic_in_forum(topic_name, data,
                                                              USER_AGENT, KEEP_UA_INFO)
        except ValueError:
            data = _search_forum(topic_name, data)
        data = ac_data
    elif forum_name and forum_page > 1:
        _, data = _winterscrape._access_forum_page(page_number, data, USER_AGENT, KEEP_UA_INFO)
        try:
            _, data = _winterscrape._access_topic_in_forum(page_number, data,
                                                           USER_AGENT, KEEP_UA_INFO)
        except ValueError:
            raise AttributeError("Topic not on forum page")
    elif forum_page < 1:
        raise ValueError("Forum Page value is less than 1")
    elif data:
        pass
    else:
        raise SyntaxError("Must pass in forum_name or data")

    if page_number > 1:
        _, data = _winterscrape._access_forum_page(page_number, data, USER_AGENT, KEEP_UA_INFO)

    data = bs4.BeautifulSoup(data, 'html5lib')
    post_data = data.find('div', id='forumposts', recursive=True)
    post_data = post_data.find('table', recursive=True)
    post_data = post_data.find_all('tr', recursive=True)

    post_dict = _get_post_data(post_data)
    return post_dict

def get_all_posts_from_topic(topic_name, forum_name=None, forum_page=1, data=None):
    """
    Gets the posts and posters from any given topic, from all pages.

    .. note:: Does not search subforums.

    .. note:: Either forum_name and forum_page must be given or data.

    :param topic_name: The name of the topic
    :type topic_name: str
    :param forum_name: The forum that the topic is located in.
    :type forum_name: str
    :param forum_page: The page of the forum that the topic is located in.
    :type forum_page: int
    :param data: The HTML data of the forum page.
    :type data: str

    :returns: dict - A dictionary of lists in the following format:
            {'username[:count]':[[Date, Time], Post_Text, Post_ID]}

    """

    post_dict = {}
    has_pages_remaining = True
    i = 0
    while has_pages_remaining and i < LOOP_LIMIT:
        try:
            new_posts = get_posts_from_topic_page(topic_name, i, forum_name, forum_page, data)
            post_dict.update(new_posts)
        except ValueError:
            has_pages_remaining = False
        i += 1

    return post_dict


def get_all_posters_from_topic_page(topic_name, page_number, forum_name=None,
                                    forum_page=1, data=None):
    """
    Gets the posters from a given topic page.

    .. note:: Does not search subforums

    .. note:: Either forum_name and forum_page must be given or data.

    :param topic_name: The name of the topic.
    :type topic_name: str
    :param page_number: The page of the topic.
    :type page_number: int
    :param forum_name: The name of the forum where the topic is located
    :type forum_name: str
    :param forum_page: The page of the forum where the topic is located
    :type forum_page: int
    :param data: HTML data of the forum's page where the topic is located
    :type data: str

    :returns: list - a list of strings containing the usernames of all posters

    """
    new_posts = get_posts_from_topic_page(topic_name, page_number, forum_name, forum_page, data)
    poster_list = list(new_posts.keys())

    poster_list = _unique_posters(poster_list)

    return poster_list


def get_all_posters_from_topic(topic_name, forum_name=None, forum_page=1, data=None):
    """
    Gets the posters in a given topic.

    .. note:: Does not search subforums

    .. note:: Either forum_name and forum_page must be given or data.

    :param topic_name: The name of the topic.
    :type topic_name: str
    :param page_number: The page of the topic.
    :type page_number: int
    :param forum_name: The name of the forum where the topic is located
    :type forum_name: str
    :param forum_page: The page of the forum where the topic is located
    :type forum_page: int
    :param data: HTML data of the forum's page where the topic is located
    :type data: str

    :returns: list - a list of strings containing the usernames of all posters

    """
    new_posts = get_all_posts_from_topic(topic_name, forum_name, forum_page, data)
    poster_list = list(new_posts.keys())

    poster_list = _unique_posters(poster_list)

    return poster_list

def get_only_posts_from_topic_page(topic_name, topic_page, forum_name=None,
                                   forum_page=1, data=None):
    """
    Gets the posts from any given topic page.

    .. note:: Does not search subforums

    .. note:: Either forum_name and forum_page must be given or data.

    :param topic_name: The name of the topic.
    :type topic_name: str
    :param page_number: The page of the topic.
    :type page_number: int
    :param forum_name: The name of the forum.
    :type forum_name: str
    :param forum_page: The page of the forum that the topic is located on.
    :type forum_page: int
    :param data: The HTML data of the forum.
    :type data: str

    :returns: list - a list of all of the text of the posts.

    """
    post_dict = get_posts_from_topic_page(topic_name, topic_page, forum_name, forum_page, data)
    post_list = list(post_dict.values())

    new_post_list = []
    for posts in post_list:
        new_post_list.append(posts[1])

    return new_post_list

def get_only_posts_from_topic(topic_name, forum_name=None, forum_page=1, data=None):
    """
    Gets the posts from any given topic, of all of the pages.

    .. note:: Does not search subforums.

    .. note:: Either forum_name and forum_page must be given or data.

    :param topic_name: The name of the topic.
    :type topic_name: str
    :param forum_name: The name of the forum.
    :type forum_name: str
    :param forum_page: The page of the forum that the topic is located on.
    :type forum_page: int
    :param data: The HTML data of the forum.
    :type data: str

    :returns: list - a list of all of the text of the posts.

    """
    post_dict = get_all_posts_from_topic(topic_name, forum_name, forum_page, data)
    post_list = list(post_dict.values())

    new_post_list = []
    for posts in post_list:
        new_post_list.append(posts[1])

    return new_post_list

def get_only_post_ids_from_topic_page(topic_name, topic_page, forum_name=None,
                                      forum_page=1, data=None):
    """
    Get the *only* the post ID's from the topic page.

    .. note:: Does not search subforums.

    .. note:: Either forum_name and forum_page must be given or data.

    :param topic_name: The name of the topic.
    :type topic_name: str
    :param forum_name: The name of the forum.
    :type forum_name: str
    :param forum_page: The page of the forum that the topic is located on.
    :type forum_page: int
    :param data: The HTML data of the forum.
    :type data: str

    :returns: list - a list of all of the text of the posts.

    """
    post_dict = get_posts_from_topic_page(topic_name, topic_page, forum_name, forum_page, data)
    post_list = list(post_dict.values())

    new_post_list = []
    for posts in post_list:
        new_post_list.append(posts[2])

    return new_post_list

def get_only_post_ids_from_topic(topic_name, forum_name=None, forum_page=1, data=None):
    """
    Get the *only* the post ID's from the topic.

    .. note:: Does not search subforums.

    .. note:: Either forum_name and forum_page must be given or data.

    :param topic_name: The name of the topic.
    :type topic_name: str
    :param forum_name: The name of the forum.
    :type forum_name: str
    :param forum_page: The page of the forum that the topic is located on.
    :type forum_page: int
    :param data: The HTML data of the forum.
    :type data: str

    :returns: list - a list of all of the text of the posts.

    """
    post_dict = get_all_posts_from_topic(topic_name, forum_name, forum_page, data)
    post_list = list(post_dict.values())

    new_post_list = []
    for posts in post_list:
        new_post_list.append(posts[2])

    return new_post_list

# bddd1b97cb0db4463c75726fecc294778fa2305b549dd46365de0482ea81b6bbc8fa7048fa6596b177a252c \
# 2008e6beab5a80f08fded1b4c4fe85096efc471ee
