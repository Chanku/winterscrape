"""
This package is a module designed to allow for better scraping and parsing of Wintreath's Forums.
"""

from . import winterscrape

__author__ = "Chanku/Sapein"
__license__ = "MIT"
__version__ = '0.0.1-alpha'
__email__ = "chandler182@gmail.com"
