#
# Copyright (C) 2017 Chanku/Sapein - All Rights Reserved
# You may use, distribute and modify this code under the
# terms of the MIT license
#
# You should have received a copy of the MIT license with
# this file. If not, please write to: chandler182@gmail.com, or
# visit https://opensource.org/licenses/MIT:
#

"""Implementation of Winterscrape"""
import urllib.request
import bs4

__author__ = "Chanku/Sapein"
__license__ = "MIT"
__version__ = '1.0.0'
__email__ = "chandler182@gmail.com"

_WINTREATH_BASE_URL = 'http://wintreath.com/forums'
_USER_AGENT = "{auth}'s winterscrape {ver} ({email})".format(auth=__author__,
                                                             ver=__version__,
                                                             email=__email__)

def _setup_connection(user_agent, wintreath_url, keep_module_info):
    global _USER_AGENT

    if user_agent != _USER_AGENT and keep_module_info:
        user_agent = "{cus_ua} using {mod_ua}".format(cus_ua=user_agent, mod_ua=_USER_AGENT)

    wintreath_request_url = urllib.request.Request(url=wintreath_url,
                                                   headers={'User-Agent':user_agent})
    _USER_AGENT = user_agent
    return wintreath_request_url

def _get_data(wintreath_url):

    with urllib.request.urlopen(wintreath_url) as wintreath_connection:
        r_wintreath_data = wintreath_connection.read()

    try:
        return r_wintreath_data, r_wintreath_data.decode('utf-8')
    except UnicodeError:
        return r_wintreath_data, r_wintreath_data.decode('ISO-8859-1')

def _initiate_connection(user_agent, wintreath_url=_WINTREATH_BASE_URL, keep_module_info=True):
    wintreath_request_url = _setup_connection(user_agent, wintreath_url, keep_module_info)
    raw_data, data = _get_data(wintreath_request_url)
    return raw_data, data

def _get_data_from_list(key, data):
    for item in data:
        if key in str(item):
            return data[data.index(item)]

def _find_topic_url_from_forum(thread_subject, data):

    wintreath_data = bs4.BeautifulSoup(data, 'html5lib')
    basic_data = wintreath_data.body.contents
    basic_data = _get_data_from_list('messageindex', basic_data)
    basic_data = _get_data_from_list('messageindex', basic_data.contents)
    basic_data = _get_data_from_list('messageindex', basic_data.contents)
    basic_data = _get_data_from_list('messageindex', basic_data.contents)
    basic_data = _get_data_from_list('messageindex', basic_data.contents)
    basic_data = _get_data_from_list('messageindex', basic_data.contents)

    tr_data = basic_data.find_all('tr')
    tr_data.pop(0)
    tr_data.pop(-1)

    for thread in tr_data:
        span = thread.find_all('span')
        for span_element in span:
            a_element = span_element.find_all('a')
            for element in a_element:
                if element.text == thread_subject:
                    topic_url = element.attrs['href']
                    return topic_url

def _find_subforum_url_from_index(subforum_name, data):

    wintreath_data = bs4.BeautifulSoup(data, 'html5lib')
    basic_data = wintreath_data.body.find_all('td', class_='children windowbg', recursive=True)

    for subforum in basic_data:
        if subforum_name.lower() in str(subforum).lower():
            basic_data = subforum
            break

    subforum_data = basic_data.div.find_all('a', recursive=True)
    for subforum in subforum_data:
        if subforum.text.strip().lower() == subforum_name.lower():
            subforum_url = subforum.attrs['href']
            return subforum_url

def _find_subfourm_url_from_forum(subforum_name, data):

    wintreath_data = bs4.BeautifulSoup(data, 'html5lib')
    basic_data = wintreath_data.body.find_all('div',
                                              class_='tborder childboards',
                                              recursive=True)[0]
    basic_data = basic_data.find_all('tbody', class_='content', recursive=True)[0]
    subforum_data = basic_data.find_all('tr')

    for subforum in subforum_data:
        name = subforum.find_all('a', class_='subject', recursive=True)[0]
        if name.text.strip() == subforum_name:
            return name.attrs['href']

def _expand_pages_implementation(base_url, first_page, last_page, per_page):
    links = []
    i = first_page
    old_last_page = 0
    per_page_limit = 50

    if (last_page - first_page) / per_page > per_page_limit:
        old_last_page = last_page
        last_page = first_page + per_page_limit * per_page

    while i < last_page:
        str_new_link = ('<a class="navPages" href="{url}">{num}</a>'
                       ).format(num=str(1+i//per_page),
                                url=base_url.replace('%1$d', str(i)).replace('%%', '%')
                               )
        new_link = bs4.BeautifulSoup(str_new_link, 'html5lib')
        new_link = new_link.find_all('a')[0]
        links.append(new_link)
        i += per_page

    if old_last_page > 0:
        new_links = _expand_pages_implementation(base_url, last_page, old_last_page, per_page)
        links.extend(new_links)

    return links

def _check_hidden_pages(small_data, page_num):
    span_data = small_data.find_all('span')[0]
    expand_pages = span_data.attrs['onclick']
    expand_pages = expand_pages.strip('expandPages(this, ').strip(");").split(',')
    base_url = expand_pages[0].strip("'")
    first_page = int(expand_pages[1])
    last_page = int(expand_pages[2])
    per_page = int(expand_pages[3])

    other_links = _expand_pages_implementation(base_url, first_page, last_page, per_page)
    for page in other_links:
        if page.text.strip() == str(page_num):
            page_url = str(page.attrs['href'])
            return page_url

def _get_topic_list(basic_data, topic):
    tr_data = basic_data.find_all('tr', recursive=True)
    tr_data.pop(0)
    tr_data.pop(-1)

    for thread in tr_data:
        span = thread.find_all('span', recursive=True)
        for span_element in span:
            a_element = span_element.find_all('a')
            for element in a_element:
                if element.text.strip() == topic:
                    tr_data = thread
                    return tr_data

def _find_topic_page_from_forum(topic, page_num, data):

    wintreath_data = bs4.BeautifulSoup(data, 'html5lib')
    basic_data = wintreath_data.body.find_all('div', id='messageindex', recursive=True)[0]
    tr_data = _get_topic_list(basic_data, topic)

    td_data = tr_data.find_all('td', recursive=True)
    for td_element in td_data:
        if "started by" in str(td_element).lower():
            td_data = td_element
            break

    small_data = td_data.find_all('small', recursive=True)[0]
    page_list = small_data.find_all('a', class_='navPages')

    for page in page_list:
        if page.text.strip() == str(page_num):
            page_url = page.attrs['href']
            return page_url

    return _check_hidden_pages(small_data, page_num)

def _find_page_url_from_topic(page_number, data):

    wintreath_data = bs4.BeautifulSoup(data, 'html5lib')
    page_data = wintreath_data.find_all('div', class_='pagesection', recursive=True)[0]
    page_data = page_data.children

    for tag in page_data:
        if "pages" in str(tag).lower():
            page_data = tag
            break

    split_num = str(page_data).split("<strong>")[1]
    split_num = split_num.split("<")[0]
    if split_num == str(page_number):
        return None

    page_list = page_data.find_all('a', class_='navPages')

    for page in page_list:
        if page.text.strip() == str(page_number):
            page_url = page.attrs['href']
            return page_url

    return _check_hidden_pages(page_data, page_number)

def _find_forum_url_from_index(forum_name, data):

    wintreath_data = bs4.BeautifulSoup(data, 'html5lib')
    basic_data = wintreath_data.body.contents
    for item in basic_data:
        if 'tbody' in str(item):
            basic_data = item
            break

    forum_data = basic_data.find_all('tbody', 'content')
    for forum in forum_data:
        forum_list = forum.find_all('a', 'subject')
        for anchor in forum_list:
            if forum_name == anchor.text.strip():
                forum_url = anchor.attrs['href'].strip()
                return forum_url

def _access_forum_area(forum_area, data, user_agent, keep_module_info=True):

    wintreath_url = _find_forum_url_from_index(forum_area, data)
    wintreath_request_url = _setup_connection(user_agent, wintreath_url, keep_module_info)
    raw_data, data = _get_data(wintreath_request_url)
    return raw_data, data

def _access_topic_in_forum(topic_name, data, user_agent, keep_module_info=True):

    wintreath_url = _find_topic_url_from_forum(topic_name, data)
    wintreath_request_url = _setup_connection(user_agent, wintreath_url, keep_module_info)
    raw_data, data = _get_data(wintreath_request_url)
    return raw_data, data


def _access_subforum_from_forum(subforum, data, user_agent, keep_module_info=True):

    wintreath_url = _find_subfourm_url_from_forum(subforum, data)
    wintreath_request_url = _setup_connection(user_agent, wintreath_url, keep_module_info)
    raw_data, data = _get_data(wintreath_request_url)
    return raw_data, data


def _access_topic_page_from_forum(topic, page_num, data, user_agent, keep_module_info=True):

    wintreath_url = _find_topic_page_from_forum(topic, page_num, data)
    wintreath_request_url = _setup_connection(user_agent, wintreath_url, keep_module_info)
    raw_data, data = _get_data(wintreath_request_url)
    return raw_data, data

def _access_forum_page(page_num, data, user_agent, keep_module_info=True):

    wintreath_url = _find_page_url_from_topic(page_num, data)
    if not wintreath_url:
        raw_data = data.encode('ISO-8859-1')
    else:
        wintreath_request_url = _setup_connection(user_agent, wintreath_url, keep_module_info)
        raw_data, data = _get_data(wintreath_request_url)
    return raw_data, data

def _access_subforum_from_index(subforum, data, user_agent, keep_module_info=True):

    wintreath_url = _find_subforum_url_from_index(subforum, data)
    wintreath_request_url = _setup_connection(user_agent, wintreath_url, keep_module_info)
    raw_data, data = _get_data(wintreath_request_url)
    return raw_data, data

#====================================================================
#NOTE: IMPLEMENTATION LINE (08.23.2017)
#NOTE: EVERYTHING BELOW THIS LINE IS NOT IMPLEMENTED
#====================================================================

def _find_user_profile(user_name, data):
    return user_name, data
    raise NotImplementedError

def _access_user_profile(user_name, data, user_agent, keep_module_info=True):
    #NOTE: Currently not possible to implement as you can't really get the user list

    wintreath_url = _find_user_profile(user_name, data)
    wintreath_request_url = _setup_connection(user_agent, wintreath_url, keep_module_info)
    raw_data, data = _get_data(wintreath_request_url)
    raise NotImplementedError
    return raw_data, data

# bddd1b97cb0db4463c75726fecc294778fa2305b549dd46365de0482ea81b6bbc8fa7048fa6596b177a252c \
# 2008e6beab5a80f08fded1b4c4fe85096efc471ee
