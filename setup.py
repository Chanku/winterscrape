from distutils.core import setup

setup(
    name="winterscrape",
    packages=['winterscrape'],
    version='0.1.0-alpha',
    author='Sapein/Chanku',
    author_email='chandle182@gmail.com',
    url='bitbucket.org/Chanku/winterscrape',
    description='A Wintreath Forum Scraping Library',
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Development Status :: 3 - Alpha",
        "Environment :: Other Environment",
        "Intended Audience :: Developers",
        "Operating System :: OS Independent",
        "Topic :: Internet",
        "Topic :: Software Development :: Libraries",
        "Topic :: Internet :: WWW/HTTP"
    ],
    license=['MIT'],
    requires=['beautifulsoup4 (== 4.6.0)',
              'html5lib (==0.999)',
             ]
    provides=['winterscrape']
)
