Resources
=========
These are the resources for Winterscrape, basically anything that can be found at it's git repo.

.. toctree::
   :maxdepth: 2

   README
   CONTRIBUTING
   code_guidelines
   CODE_OF_CONDUCT
   TODO
   LICENSE
   CHANGELOG


