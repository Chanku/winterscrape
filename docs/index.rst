.. Winterscrape documentation master file, created by
   sphinx-quickstart on Sat Sep  2 00:02:58 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Winterscrape's documentation!
========================================
Welcome to the Winterscrape Docs!

The module section contains the Code Documentation and the resources file contains everything else.

.. toctree::
   :maxdepth: 2

   source/modules
   source/resources


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

