Winterscrape
============

   Winterscrape is a Python Library/Module designed to be used to scrape Wintreath's forums

[![Documentation Status](https://readthedocs.org/projects/winterscrape/badge/?version=latest)](http://winterscrape.readthedocs.io/en/latest/?badge=latest)

### Information  
Version: 0.1.0-Alpha

### Requirements 
1. Python 3.X (Python 3.4 recommended)
2. Beautiful Soup 4
3. html5lib
4. Sphinx (If you wish to build documentation yourself)

### Installation 
1. Perform `git clone https://Chanku@bitbucket.org/Chanku/winterscrape.git`
2. Do `cd winterscrape`
3. Do `pip3 install .` 
4. Import the package and the winterscrape module (`import winterscrape.winterscrape`)

### Usage
1. Install the project
2. Import the Winterscrape module from the Winterscrape package (winterscrape.winterscrape)
3. Refer to the documentation

### Removal
Just run `pip3 uninstall winterscrape`

### Contributing 
   If you wish to contribute then please read the CONTRIBUTING file

### Copyright and Licensing  
   Copyright 2017, Chanku/Sapein, all other copyright belongs to their respective copyright holders. This project is licensed under the MIT License, unless otherwise stated



### Q&A (or PFAQ (Potentially Frequently Asked Questions)  

#### Can I use this with Python 2?  
   You can try, although I make no guarentees that it will work on Python 2 and I won't offer support directly for Python 2. Although I don't use many Python 3 only features, so you might be able to use it with minimal issues. Although I have tested some things in Python 2.7

#### Why did you make this?  
   I was/am working on another project that needed this, I figured I might as well make a basic-ish library that can handle it

#### What's a Wintreath?  
   Wintreath is a region (and thus community) on NationStates, which has it's own offsite-forum.

#### Can I use this to scrape [INSERT FORUM HERE] on the Simple Machine Forums Software?  
   Potentially, although this is going to be maintained among Wintreath's forums, so if Wintreath changes something that SMF doesn't do, it will use the Wintreath behavior instead. If your forum makes changes that are different than Wintreath's changes, but in the areas the forum checks, it will probably not work.

#### Can I use this to scrape [INSERT FORUM HERE] on [INSERT OTHER FORUM SOFTWARE HERE]?  
   Probably not, it's pretty specifically tailored to Wintreath's forums, which uses SMF. You can try it, but don't expect much from it.

#### Can I submit a patch to make it work on [INSERT FORUM HERE]?  
   Only if it doesn't negatively impact the scraping of Wintreath's forums?

#### What is the \_winterscrape.py file?  
   It's the Private API, don't import it directly. The Public API (winterscrape.py) was designed to support most of what you want.

#### Why are the API's separated?
   So I can mess with internals without messing with the Public API as much. Further, the file separation allows me to be better handle versioning, and makes both cleaner.

#### What if Wintreath moves away from SMF?  
   Then the internal API will be rewritten to follow the new forum software and this will be updated to replace some of the specific SMF stuff to the new forum software.

#### What if Wintreath develops an API that scraping useless?  
   Then I will rewrite the Internal API to use that new API. If I keep the old Internal API around, it will be either in a separate branch, project, or module. Don't expect it to be updated after the API is released though.

#### Can I contribute?  
   Sure, read CONTRIBUTING

#### Why do you let [INSERT PERSON HERE] contribute?  
   I don't care about a person, so long as their code is decent and isn't shit.

#### Where's the Documentation?  
   I'm writing it, I'm focusing on getting it working first though.

#### Can I submit a patch through e-mail?  
   Yes! If you don't want to use bitbucket, for whatever reason, you can just email it to me instead, my email can be found within the source files. Make sure you follow the Request Guidelines and Requirements in CONTRIBUTING though.

#### Where can I get the Changelog?
   The CHANGELOG file, although please note that at the release of 1.0 the CHANGELOG file will be wiped, and will only be updated on official releases.

bddd1b97cb0db4463c75726fecc294778fa2305b549dd46365de0482ea81b6bbc8fa7048fa6596b177a252c2008e6beab5a80f08fded1b4c4fe85096efc471ee
